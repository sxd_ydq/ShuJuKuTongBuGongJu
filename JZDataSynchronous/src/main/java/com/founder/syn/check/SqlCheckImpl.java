package com.founder.syn.check;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.founder.syn.log.LoggerUtil;
 
/** 
* @author  作者 E-mail: 
* @date 创建时间：2018年1月30日 下午6:21:28 
* @version 1.0 
* @parameter  
* @since  
* @return  
*/
@Component
public  class SqlCheckImpl  implements SqlCheck {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	 
	public void sql(String sql) {
		// TODO 自动生成的方法存根
		 
		
			jdbcTemplate.update(sql);
		 
	}
	 
	 
}
