package com.founder.syn.dao;

import com.founder.syn.entity.Table;

/**
 * @author 作者 xc E-mail:
 * @date 创建时间：2018年1月26日 下午1:57:39
 * @version 1.0
 * @parameter
 * @since
 * @return
 */
public interface SynDao {

	public void delete(Table table)throws Exception ;  

	public void replace(Table table) throws Exception;

	public void insert(Table table)throws Exception ;  

	public int select(Table table);
}
