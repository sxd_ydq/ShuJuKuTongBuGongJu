package com.founder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.EnableTransactionManagement;


/** 
* @author  作者 xc  E-mail: 
* @date 创建时间：2018年1月26日 下午2:01:17 
* @version 1.0 
* @parameter  
* @since  应用启动
* @return  
*/
  
@SpringBootApplication
public class Application {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
